import numpy as np
import tensorflow as tf
import Image
import os.path

import matplotlib.pyplot as plt


np.random.seed(0)
tf.set_random_seed(0)


def rgb2gray(rgb):
    return np.expand_dims(np.dot(rgb[..., :3], [0.299, 0.587, 0.114]), axis=2)


def invert(rgb):
    temp = rgb[..., 0]
    rgb[..., 0] = rgb[..., 2]
    rgb[..., 2] = temp


def divide(img, w, h, num):
    skip = 1
    rows = img.shape[0] / h
    cols = img.shape[1] / w
    data = np.empty([num, h, w, 3], dtype=float)
    count = 0
    for i in range(1, rows, skip):
        for j in range(0, cols, skip):
            data[count, ...] = img[(i * h):((i + 1) * h), (j * w):((j + 1) * w), ...]
            count = count + 1
            if count >= num:
                return data
    return data

prefix = "/home/tao/Desktop/WaterReplicator/data"
n_images = 7
width = 200
height = 150

dataset = np.zeros((n_images, height, width, 3), dtype=float)
label = np.zeros((n_images, height, width, 3), dtype=float)

index = 0
for i in range(0, n_images, 1):
    img = Image.open(prefix + '/raw_' + str(i) + '.JPG')
    img.thumbnail((width, height), Image.ANTIALIAS)
    pref = Image.open(prefix + '/pref_' + str(i) + '.JPG')
    pref.thumbnail((width, height), Image.ANTIALIAS)
    dataset[index, ...] = np.asarray(img) / 255.0
    label[index, ...] = np.asarray(pref) / 255.0
    index = index + 1

plt.figure(1)
plt.imshow(dataset[3, ...])
plt.figure(2)
plt.imshow(label[3, ...])

plt.ion()
plt.show()
key_input = raw_input("enter")
print ">>>", len(key_input)


def xavier_init(h, w, fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((h, w, fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class Painter(object):

    def __init__(self, network_architecture, transfer_fct=tf.nn.relu6, learning_rate=0.001):
        self.network_architecture = network_architecture
        self.transfer_fct = transfer_fct
        self.learning_rate = learning_rate

        print network_architecture.viewkeys()
        self.y = tf.placeholder(tf.float32, [height, width, 3])

        self.A1_in = tf.placeholder(tf.float32, [3, 3])
        self.A2_in = tf.placeholder(tf.float32, [network_architecture['n_features'][0], network_architecture['n_features'][0]])
        self.A3_in = tf.placeholder(tf.float32, [network_architecture['n_features'][1], network_architecture['n_features'][1]])
        self.h1_in = tf.placeholder(tf.float32, [1, height, width, network_architecture['n_features'][0]])
        self.h2_in = tf.placeholder(tf.float32, [1, height, width, network_architecture['n_features'][1]])
        self.h3_in = tf.placeholder(tf.float32, [1, height, width, network_architecture['n_features'][2]])

        self._create_network()
        self._create_loss_optimizer()

        init = tf.initialize_all_variables()

        layer1_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer1")
        self.saver1 = tf.train.Saver(var_list=layer1_)
        layer2_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer2")
        self.saver2 = tf.train.Saver(var_list=layer2_)
        layer3_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer3")
        self.saver3 = tf.train.Saver(var_list=layer3_)

        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self):
        self.network_weights = self._initialize_weights(**self.network_architecture)
        inputY = tf.reshape(self.y, [1, height, width, 3])
        (self.h1, self.A1, self.loss1) = self._auto_encoder(inputY, self.network_weights['w']['1'], self.network_weights['g']['1'])
        (self.h2, self.A2, self.loss2) = self._auto_encoder(self.h1, self.network_weights['w']['2'], self.network_weights['g']['2'])
        (self.h3, self.A3, self.loss3) = self._auto_encoder(self.h2, self.network_weights['w']['3'], self.network_weights['g']['3'])

        self.h2_in = self._generative_network(self.A3_in, self.h3_in, self.network_weights['w']['3'], self.network_weights['g']['3'])
        self.h1_in = self._generative_network(self.A2_in, self.h2_in, self.network_weights['w']['2'], self.network_weights['g']['2'])
        self.h0_in = self._generative_network(self.A1_in, self.h1_in, self.network_weights['w']['1'], self.network_weights['g']['1'])
        self.gen_output = tf.reshape(self.h0_in, [height, width, 3])

    def _initialize_weights(self, n_features, k_width, k_height):
        all_weights = dict()
        all_weights['w'] = {
            '1': tf.Variable(xavier_init(k_height, k_width, 3, n_features[0]), name="layer1/w1"),
            '2': tf.Variable(xavier_init(k_height + 2, k_width + 2, n_features[0], n_features[1]), name="layer2/w2"),
            '3': tf.Variable(xavier_init(k_height + 4, k_width + 4, n_features[1], n_features[2]), name="layer3/w3")}
        all_weights['g'] = {
            '3': tf.Variable(xavier_init(k_height + 4, k_width + 4, n_features[2], n_features[1]), name="layer3/g3"),
            '2': tf.Variable(xavier_init(k_height + 2, k_width + 2, n_features[1], n_features[0]), name="layer2/g2"),
            '1': tf.Variable(xavier_init(k_height, k_width, n_features[0], 3), name="layer1/g1")}
        return all_weights

    def _auto_encoder(self, in_tensor, w, g):

        v = tf.reshape(in_tensor, [width * height, -1])
        A = tf.matmul(v, v, True)

        h = self.transfer_fct(tf.nn.conv2d(in_tensor, w, [1, 1, 1, 1], "SAME"))

        out = self._generative_network(A, h, w, g)
        # loss = tf.reduce_mean(tf.square(v - out))
        loss = tf.reduce_sum(tf.square(v - out))

        return h, A, loss

    def _generative_network(self, A, hs, w, g):

        h = tf.reshape(hs, [1, height, width, -1])

        DE = tf.self_adjoint_eig(A)
        D = tf.diag(tf.sqrt(tf.maximum(tf.squeeze(tf.slice(DE, [0, 0], [1, -1])), 0)))
        E = tf.slice(DE, [1, 0], [-1, -1])
        v_ = tf.matmul(D, E, False, True)
        # v_ = tf.transpose(tf.cholesky(A))

        r = tf.nn.conv2d(h, g, [1, 1, 1, 1], "SAME")
        R = tf.reshape(r, [width * height, -1])
        out = tf.matmul(R, v_, False)

        return out

    def _create_loss_optimizer(self):

        layer1_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer1")
        self.optimizer1 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss1, var_list=layer1_)

        layer2_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer2")
        self.optimizer2 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss2, var_list=layer2_)

        layer3_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer3")
        self.optimizer3 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss3, var_list=layer3_)

    def fine_tune_1(self, Y):
        opt, loss = self.sess.run((self.optimizer1, self.loss1), feed_dict={self.y: Y})
        return loss

    def fine_tune_2(self, Y):
        opt, loss = self.sess.run((self.optimizer2, self.loss2), feed_dict={self.y: Y})
        return loss

    def fine_tune_3(self, Y):
        opt, loss = self.sess.run((self.optimizer3, self.loss3), feed_dict={self.y: Y})
        return loss

    def get_style(self, Y):
        return self.sess.run((self.A1, self.A2, self.A3), feed_dict={self.y: Y})

    def get_content(self, Y):
        return self.sess.run((self.h1, self.h2, self.h3), feed_dict={self.y: Y})

    def transform(self, content, style):
        (A1, A2, A3) = self.get_style(style)
        (h1, h2, h3) = self.get_content(content)
        return self.sess.run(self.gen_output, feed_dict={self.A1_in: A1, self.A2_in: A2, self.A3_in: A3, self.h3_in: h3})

    def terminate(self):
        self.sess.close()

    def save1(self, file):
        self.saver1.save(self.sess, file)

    def save2(self, file):
        self.saver2.save(self.sess, file)

    def save3(self, file):
        self.saver3.save(self.sess, file)

    def load1(self, file):
        if os.path.isfile(file):
            self.saver1.restore(self.sess, file)
            return True
        else:
            return False

    def load2(self, file):
        if os.path.isfile(file):
            self.saver2.restore(self.sess, file)
            return True
        else:
            return False

    def load3(self, file):
        if os.path.isfile(file):
            self.saver3.restore(self.sess, file)
            return True
        else:
            return False


def train(network_architecture, learning_rate=0.01, batch_size=1, training_epochs=1000, display_step=10):
    network = Painter(network_architecture, learning_rate=learning_rate)

    if not network.load1("./decom/model1.weights") or (len(key_input) >= 1 and key_input[0] == 't'):
        print "training layer 1 ..."
        prev_loss = 0.
        for epoch in range(training_epochs):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                batch_ys = dataset[i, ...]
                loss = network.fine_tune_1(batch_ys)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save1("./decom/model1.weights")

    if not network.load2("./decom/model2.weights") or (len(key_input) >= 2 and key_input[1] == 't'):
        print "training layer 2 ..."
        prev_loss = 0.
        for epoch in range(training_epochs):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                batch_ys = dataset[i, ...]
                loss = network.fine_tune_2(batch_ys)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save2("./decom/model2.weights")

    if not network.load3("./decom/model3.weights") or (len(key_input) >= 3 and key_input[2] == 't'):
        print "training layer 3 ..."
        prev_loss = 0.
        for epoch in range(training_epochs):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                batch_ys = dataset[i, ...]
                loss = network.fine_tune_3(batch_ys)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save3("./decom/model3.weights")

    return network

network_architecture = dict(n_features=[30, 60, 90],
                            k_width=3,
                            k_height=3)

painter = train(network_architecture, training_epochs=10000)

pref_test = Image.open(prefix + '/pref_6.JPG')
pref_test.thumbnail((width, height), Image.ANTIALIAS)
style = np.asarray(pref_test) / 255.0

test = Image.open(prefix + '/raw_7.JPG')
test.thumbnail((width, height), Image.ANTIALIAS)
location = np.asarray(test) / 255.0

output_test = painter.transform(style, style)
# output_test = painter.self_generate(location)

plt.figure(1)
plt.imshow(location)

plt.figure(2)
plt.imshow(style)

plt.figure(3)
plt.imshow(output_test)

plt.show()
raw_input("enter")


painter.terminate()
