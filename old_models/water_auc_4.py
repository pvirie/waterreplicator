import numpy as np
import tensorflow as tf
import Image
import os.path

import matplotlib.pyplot as plt


np.random.seed(0)
tf.set_random_seed(0)

n_samples = 100
width = 200
height = 150


def rgb2gray(rgb):
    return np.expand_dims(np.dot(rgb[..., :3], [0.299, 0.587, 0.114]), axis=2)


def divide(img, w, h, num):
    skip = 1
    rows = img.shape[0] / h
    cols = img.shape[1] / w
    data = np.empty([num, h, w, 3], dtype=float)
    count = 0
    for i in range(1, rows, skip):
        for j in range(0, cols, skip):
            data[count, ...] = img[(i * h):((i + 1) * h), (j * w):((j + 1) * w), ...]
            count = count + 1
            if count >= num:
                return data
    return data

dataset = np.zeros((n_samples, height, width, 3), dtype=float)
label = np.zeros((n_samples, height, width, 3), dtype=float)

prefix = "/home/tao/Desktop/WaterReplicator/data"
n_images = 5
n_sample_per_image = 20

index = 0
for i in range(0, n_images, 1):
    img = Image.open(prefix + '/raw_' + str(i) + '.JPG')
    pref = Image.open(prefix + '/pref_' + str(i) + '.JPG')
    dataset[index:(index + n_sample_per_image), ...] = divide(np.asarray(img) / 255.0, width, height, n_sample_per_image)
    label[index:(index + n_sample_per_image), ...] = divide(np.asarray(pref) / 255.0, width, height, n_sample_per_image)
    index = index + n_sample_per_image

plt.figure(1)
plt.imshow(dataset[6, ...])
plt.figure(2)
plt.imshow(label[6, ...])

plt.ion()
plt.show()
key_input = raw_input("enter")
print ">>>", len(key_input)


def xavier_init(h, w, fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    # https://stackoverflow.com/questions/33640581/how-to-do-xavier-initialization-on-tensorflow
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((h, w, fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


def whitenoise_init(h, w, d, constant=0.1):
    return tf.random_uniform((h, w, d), minval=0.0, maxval=constant, dtype=tf.float32)


class Painter(object):

    def __init__(self, network_architecture, transfer_fct=tf.nn.relu6, learning_rate=0.001):
        self.network_architecture = network_architecture
        self.transfer_fct = transfer_fct
        self.learning_rate = learning_rate

        print network_architecture.viewkeys()
        self.y = tf.placeholder(tf.float32, [height, width, 3])
        self.k_width = network_architecture["k_width"]
        self.k_height = network_architecture["k_height"]

        self.x = tf.Variable(whitenoise_init(height, width, 3), name="input/image")
        self.tx = tf.placeholder(tf.float32, [height, width, 3])
        self.tf1 = tf.placeholder(tf.float32, [1, height, width, self.network_architecture['n_hidden_recog_1']])
        self.tf2 = tf.placeholder(tf.float32, [1, height, width, self.network_architecture['n_hidden_recog_2']])
        self.tf3 = tf.placeholder(tf.float32, [1, height, width, self.network_architecture['n_hidden_recog_3']])
        self.ts1 = tf.placeholder(tf.float32, [self.network_architecture['n_hidden_recog_1'], self.network_architecture['n_hidden_recog_1']])
        self.ts2 = tf.placeholder(tf.float32, [self.network_architecture['n_hidden_recog_2'], self.network_architecture['n_hidden_recog_2']])
        self.ts3 = tf.placeholder(tf.float32, [self.network_architecture['n_hidden_recog_3'], self.network_architecture['n_hidden_recog_3']])

        self._create_network()
        self._create_loss_optimizer()
        self._create_generative_optimizer()

        init = tf.initialize_all_variables()

        layer1_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer1")
        self.saver1 = tf.train.Saver(var_list=layer1_)
        layer2_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer2")
        self.saver2 = tf.train.Saver(var_list=layer2_)
        layer3_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer3")
        self.saver3 = tf.train.Saver(var_list=layer3_)

        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self):
        self.network_weights = self._initialize_weights(**self.network_architecture)
        (self.yp1, self.yp2, self.yp3) = self._auto_encoder(self.network_weights['recog'], self.network_weights['gen'])
        self._style_network(**self.network_architecture)
        self._generate_network(self.network_weights['recog'], self.network_weights['gen'],
                               self.network_architecture['n_hidden_recog_1'], self.network_architecture['n_hidden_recog_2'], self.network_architecture['n_hidden_recog_3'])

    def _initialize_weights(self, n_hidden_recog_1, n_hidden_recog_2, n_hidden_recog_3, k_width, k_height):
        all_weights = dict()
        all_weights['recog'] = {
            'h1': tf.Variable(xavier_init(k_height, k_width, 3, n_hidden_recog_1), name="layer1/h1"),
            'h2': tf.Variable(xavier_init(k_height + 2, k_width + 2, n_hidden_recog_1, n_hidden_recog_2), name="layer2/h2"),
            'h3': tf.Variable(xavier_init(k_height + 4, k_width + 4, n_hidden_recog_2, n_hidden_recog_3), name="layer3/h3")}
        all_weights['gen'] = {
            'h3': tf.Variable(xavier_init(k_height + 4, k_width + 4, n_hidden_recog_3, n_hidden_recog_2), name="layer3/g3"),
            'h2': tf.Variable(xavier_init(k_height + 2, k_width + 2, n_hidden_recog_2, n_hidden_recog_1), name="layer2/g2"),
            'h1': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_1, 3), name="layer1/g1")}
        return all_weights

    def _auto_encoder(self, recog, gen):

        inputY = tf.reshape(self.y, [1, height, width, 3])

        self.r_1 = self.transfer_fct(tf.nn.conv2d(inputY, recog["h1"], [1, 1, 1, 1], "SAME"))
        temp11_ = tf.nn.sigmoid(tf.nn.conv2d(self.r_1, gen["h1"], [1, 1, 1, 1], "SAME"))
        out1 = tf.reshape(temp11_, [height, width, 3])

        self.r_2 = self.transfer_fct(tf.nn.conv2d(self.r_1, recog["h2"], [1, 1, 1, 1], "SAME"))
        temp21_ = self.transfer_fct(tf.nn.conv2d(self.r_2, gen["h2"], [1, 1, 1, 1], "SAME"))
        temp22_ = tf.nn.sigmoid(tf.nn.conv2d(temp21_, gen["h1"], [1, 1, 1, 1], "SAME"))
        out2 = tf.reshape(temp22_, [height, width, 3])

        self.r_3 = self.transfer_fct(tf.nn.conv2d(self.r_2, recog["h3"], [1, 1, 1, 1], "SAME"))
        temp31_ = self.transfer_fct(tf.nn.conv2d(self.r_3, gen["h3"], [1, 1, 1, 1], "SAME"))
        temp32_ = self.transfer_fct(tf.nn.conv2d(temp31_, gen["h2"], [1, 1, 1, 1], "SAME"))
        temp33_ = tf.nn.sigmoid(tf.nn.conv2d(temp32_, gen["h1"], [1, 1, 1, 1], "SAME"))
        out3 = tf.reshape(temp33_, [height, width, 3])

        return out1, out2, out3

    def _style_network(self, n_hidden_recog_1, n_hidden_recog_2, n_hidden_recog_3, k_width, k_height):
        rf_1 = tf.reshape(self.r_1, [height * width, n_hidden_recog_1])
        self.s_1 = tf.matmul(rf_1, rf_1, True)
        rf_2 = tf.reshape(self.r_2, [height * width, n_hidden_recog_2])
        self.s_2 = tf.matmul(rf_2, rf_2, True)
        rf_3 = tf.reshape(self.r_3, [height * width, n_hidden_recog_3])
        self.s_3 = tf.matmul(rf_3, rf_3, True)

    def _generate_network(self, recog, gen, n_hidden_recog_1, n_hidden_recog_2, n_hidden_recog_3):
        inputX = tf.reshape(self.x, [1, height, width, 3])

        self.f_1 = self.transfer_fct(tf.nn.conv2d(inputX, recog["h1"], [1, 1, 1, 1], "SAME"))
        self.f_2 = self.transfer_fct(tf.nn.conv2d(self.f_1, recog["h2"], [1, 1, 1, 1], "SAME"))
        self.f_3 = self.transfer_fct(tf.nn.conv2d(self.f_2, recog["h3"], [1, 1, 1, 1], "SAME"))
        rf_1 = tf.reshape(self.f_1, [height * width, n_hidden_recog_1])
        self.t_1 = tf.matmul(rf_1, rf_1, True)
        rf_2 = tf.reshape(self.f_2, [height * width, n_hidden_recog_2])
        self.t_2 = tf.matmul(rf_2, rf_2, True)
        rf_3 = tf.reshape(self.f_3, [height * width, n_hidden_recog_3])
        self.t_3 = tf.matmul(rf_3, rf_3, True)

    def _create_loss_optimizer(self):

        layer1_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer1")
        error1 = -tf.add(tf.mul(self.y, tf.log(1e-10 + self.yp1)), tf.mul(1 - self.y, tf.log(1e-10 + 1 - self.yp1)))
        self.loss1 = tf.reduce_mean(error1)
        self.optimizer1 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss1, var_list=layer1_)

        layer2_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer2")
        error2 = -tf.add(tf.mul(self.y, tf.log(1e-10 + self.yp2)), tf.mul(1 - self.y, tf.log(1e-10 + 1 - self.yp2)))
        self.loss2 = tf.reduce_mean(error2)
        self.optimizer2 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss2, var_list=layer2_)

        layer3_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer3")
        error3 = -tf.add(tf.mul(self.y, tf.log(1e-10 + self.yp3)), tf.mul(1 - self.y, tf.log(1e-10 + 1 - self.yp3)))
        self.loss3 = tf.reduce_mean(error3)
        self.optimizer3 = tf.train.GradientDescentOptimizer(learning_rate=self.learning_rate).minimize(self.loss3, var_list=layer3_)

    def _create_generative_optimizer(self):

        raw_input_only = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "input")
        content_loss = tf.reduce_sum(tf.square(self.f_1 - self.tf1))
        style_loss = tf.reduce_mean(tf.square(self.t_1 - self.ts1)) + tf.reduce_mean(tf.square(self.t_2 - self.ts2)) + tf.reduce_mean(tf.square(self.t_3 - self.ts3))
        # self.gen_loss = content_loss + 1.0 * style_loss
        # self.gen_loss = content_loss
        self.gen_loss = style_loss
        self.generative_optimizer = tf.train.AdagradOptimizer(learning_rate=0.1).minimize(self.gen_loss, var_list=raw_input_only)

    def get_response(self, Y):
        return self.sess.run((self.r_1, self.r_2, self.r_3), feed_dict={self.y: Y})

    def get_style(self, Y):
        return self.sess.run((self.s_1, self.s_2, self.s_3), feed_dict={self.y: Y})

    def generate(self, content, style):
        (pr1, pr2, pr3) = self.get_response(content)
        (ps1, ps2, ps3) = self.get_style(style)

        prev_loss = 0.
        for i in range(5000):
            opt, avg_loss = self.sess.run((self.generative_optimizer, self.gen_loss),
                                          feed_dict={self.tx: content, self.tf1: pr1, self.tf2: pr2, self.tf3: pr3, self.ts1: ps1, self.ts2: ps2, self.ts3: ps3})
            if i % 10 == 0:
                print "step:", '%04d' % (i + 1), \
                    "loss=", "{:.9f}".format(avg_loss)
            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        return self.sess.run(self.x)

    def fine_tune_1(self, Y):
        opt, loss = self.sess.run((self.optimizer1, self.loss1), feed_dict={self.y: Y})
        return loss

    def fine_tune_2(self, Y):
        opt, loss = self.sess.run((self.optimizer2, self.loss2), feed_dict={self.y: Y})
        return loss

    def fine_tune_3(self, Y):
        opt, loss = self.sess.run((self.optimizer3, self.loss3), feed_dict={self.y: Y})
        return loss

    def transform_1(self, Y):
        return self.sess.run(self.yp1, feed_dict={self.y: Y})

    def transform_2(self, Y):
        return self.sess.run(self.yp2, feed_dict={self.y: Y})

    def transform_3(self, Y):
        return self.sess.run(self.yp3, feed_dict={self.y: Y})

    def terminate(self):
        self.sess.close()

    def save1(self, file):
        self.saver1.save(self.sess, file)

    def save2(self, file):
        self.saver2.save(self.sess, file)

    def save3(self, file):
        self.saver3.save(self.sess, file)

    def load1(self, file):
        if os.path.isfile(file):
            self.saver1.restore(self.sess, file)
            return True
        else:
            return False

    def load2(self, file):
        if os.path.isfile(file):
            self.saver2.restore(self.sess, file)
            return True
        else:
            return False

    def load3(self, file):
        if os.path.isfile(file):
            self.saver3.restore(self.sess, file)
            return True
        else:
            return False


def train(network_architecture, learning_rate=0.1, batch_size=1, training_epochs=10, display_step=1):
    network = Painter(network_architecture, learning_rate=learning_rate)

    if len(key_input) >= 3 or not network.load1("./weights/model1.weights"):
        print "training layer 1 ..."
        prev_loss = 0.
        for epoch in range(training_epochs):
            avg_loss = 0.
            total_batch = n_samples
            for i in range(total_batch):
                # batch_xs = dataset[i, ...]
                batch_ys = label[i, ...]
                loss = network.fine_tune_1(batch_ys)
                avg_loss += loss / n_samples

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save1("./weights/model1.weights")

    if len(key_input) >= 2 or not network.load2("./weights/model2.weights"):
        print "training layer 2 ..."
        prev_loss = 0.
        for epoch in range(training_epochs):
            avg_loss = 0.
            total_batch = n_samples
            for i in range(total_batch):
                # batch_xs = dataset[i, ...]
                batch_ys = label[i, ...]
                loss = network.fine_tune_2(batch_ys)
                avg_loss += loss / n_samples

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save2("./weights/model2.weights")

    if len(key_input) >= 1 or not network.load3("./weights/model3.weights"):
        print "training layer 3 ..."
        prev_loss = 0.
        for epoch in range(training_epochs):
            avg_loss = 0.
            total_batch = n_samples
            for i in range(total_batch):
                # batch_xs = dataset[i, ...]
                batch_ys = label[i, ...]
                loss = network.fine_tune_3(batch_ys)
                avg_loss += loss / n_samples

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save3("./weights/model3.weights")

    return network


network_architecture = \
    dict(n_hidden_recog_1=100,  # 1st layer encoder neurons
         n_hidden_recog_2=200,  # 2nd layer encoder neurons
         n_hidden_recog_3=400,  # 2nd layer encoder neurons
         k_width=3,
         k_height=3)

painter = train(network_architecture, training_epochs=1000)

pref_test = Image.open(prefix + '/pref_7.JPG')
pref_test.thumbnail((width, height), Image.ANTIALIAS)
img_pref = np.asarray(pref_test) / 255.0

test = Image.open(prefix + '/raw_4.JPG')
test.thumbnail((width, height), Image.ANTIALIAS)
img_test = np.asarray(test) / 255.0

location = img_test
style = img_pref

output_test = painter.generate(location, style)
# output_test = painter.transform_3(style)

plt.figure(1)
plt.imshow(location)

plt.figure(2)
plt.imshow(style)

plt.figure(3)
plt.imshow(output_test)

plt.show()
raw_input("enter")

painter.terminate()
