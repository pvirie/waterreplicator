import numpy as np
import tensorflow as tf
import Image
import os.path

import matplotlib.pyplot as plt


np.random.seed(0)
tf.set_random_seed(0)


def rgb2gray(rgb):
    return np.expand_dims(np.dot(rgb[..., :3], [0.299, 0.587, 0.114]), axis=2)


def invert(rgb):
    temp = rgb[..., 0]
    rgb[..., 0] = rgb[..., 2]
    rgb[..., 2] = temp


def divide(img, w, h, num):
    skip = 1
    rows = img.shape[0] / h
    cols = img.shape[1] / w
    data = np.empty([num, h, w, 3], dtype='float32')
    count = 0
    for i in range(1, rows, skip):
        for j in range(0, cols, skip):
            data[count, ...] = img[(i * h):((i + 1) * h), (j * w):((j + 1) * w), ...]
            count = count + 1
            if count >= num:
                return data
    return data

prefix = "/home/pvirie/Desktop/waterreplicator/data"
n_images = 14
width = 200
height = 150

dataset = np.zeros((n_images, height, width, 3), dtype='float32')

index = 0
for i in range(0, n_images / 2, 1):
    img = Image.open(prefix + '/raw_' + str(i) + '.JPG')
    img.thumbnail((width, height), Image.ANTIALIAS)
    pref = Image.open(prefix + '/pref_' + str(i) + '.JPG')
    pref.thumbnail((width, height), Image.ANTIALIAS)
    dataset[index, ...] = np.asarray(img) / 255.0
    dataset[index + 1, ...] = np.asarray(pref) / 255.0
    index = index + 2

plt.figure(1)
plt.imshow(dataset[3, ...])

plt.ion()
plt.show()
key_input = raw_input("enter")
print ">>>", len(key_input)


def xavier_init(h, w, fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((h, w, fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class Painter(object):

    def __init__(self, network_architecture, learning_rate=0.001):
        self.network_architecture = network_architecture
        self.learning_rate = learning_rate

        print network_architecture.viewkeys()
        self.y = tf.placeholder(tf.float32, [height, width, 3])

        self.v1_in = tf.placeholder(tf.float32, [3, 3])
        self.v2_in = tf.placeholder(tf.float32, [network_architecture['n_features'][0], network_architecture['n_features'][0]])
        self.v3_in = tf.placeholder(tf.float32, [network_architecture['n_features'][1], network_architecture['n_features'][1]])
        self.h1_in = tf.placeholder(tf.float32, [height, width, network_architecture['n_features'][0]])
        self.h2_in = tf.placeholder(tf.float32, [height, width, network_architecture['n_features'][1]])
        self.h3_in = tf.placeholder(tf.float32, [height, width, network_architecture['n_features'][2]])

        self._create_network()
        self._create_loss_optimizer()

        init = tf.initialize_all_variables()

        layer1_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer1")
        self.saver1 = tf.train.Saver(var_list=layer1_)
        layer2_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer2")
        self.saver2 = tf.train.Saver(var_list=layer2_)
        layer3_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer3")
        self.saver3 = tf.train.Saver(var_list=layer3_)

        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self):
        self.network_weights = self._initialize_weights(**self.network_architecture)
        (self.h1, self.loss1, out1) = self._auto_encoder(self.y, self.v1_in, self.network_weights['w']['1'], self.network_weights['w2']['1'], self.network_weights['g']['1'], self.network_weights['g2']['1'])
        (self.h2, loss2_, out2) = self._auto_encoder(self.h1, self.v2_in, self.network_weights['w']['2'], self.network_weights['w2']['2'], self.network_weights['g']['2'], self.network_weights['g2']['2'])
        (self.h3, self.loss3, out3) = self._auto_encoder(self.h2, self.v3_in, self.network_weights['w']['3'], self.network_weights['w2']['3'], self.network_weights['g']['3'], self.network_weights['g2']['3'])

        out1_ = self._generative_network(self.v1_in, out2, self.network_weights['w']['1'], self.network_weights['w2']['1'], self.network_weights['g']['1'], self.network_weights['g2']['1'])
        v = tf.reshape(self.y, [width * height, -1])
        self.loss2 = tf.reduce_sum(tf.square(v - out1_))

        self.h2_in = self._generative_network(self.v3_in, self.h3_in, self.network_weights['w']['3'], self.network_weights['w2']['3'], self.network_weights['g']['3'], self.network_weights['g2']['3'])
        self.h1_in = self._generative_network(self.v2_in, self.h2_in, self.network_weights['w']['2'], self.network_weights['w2']['2'], self.network_weights['g']['2'], self.network_weights['g2']['2'])
        self.h0_in = self._generative_network(self.v1_in, self.h1_in, self.network_weights['w']['1'], self.network_weights['w2']['1'], self.network_weights['g']['1'], self.network_weights['g2']['1'])
        self.gen_output = tf.reshape(self.h0_in, [height, width, 3])

    def _initialize_weights(self, n_features, k_width, k_height):
        all_weights = dict()
        all_weights['w'] = {
            '1': tf.Variable(xavier_init(k_height, k_width, 3, n_features[0]), name="layer1/w1"),
            '2': tf.Variable(xavier_init(k_height, k_width, n_features[0], n_features[1]), name="layer2/w2"),
            '3': tf.Variable(xavier_init(k_height, k_width, n_features[1], n_features[2]), name="layer3/w3")}
        all_weights['w2'] = {
            '1': tf.Variable(xavier_init(k_height, k_width, n_features[0], n_features[0]), name="layer1/w21"),
            '2': tf.Variable(xavier_init(k_height, k_width, n_features[1], n_features[1]), name="layer2/w22"),
            '3': tf.Variable(xavier_init(k_height, k_width, n_features[2], n_features[2]), name="layer3/w23")}
        all_weights['g'] = {
            '3': tf.Variable(xavier_init(k_height, k_width, n_features[2], n_features[1]), name="layer3/g3"),
            '2': tf.Variable(xavier_init(k_height, k_width, n_features[1], n_features[0]), name="layer2/g2"),
            '1': tf.Variable(xavier_init(k_height, k_width, n_features[0], 3), name="layer1/g1")}
        all_weights['g2'] = {
            '3': tf.Variable(xavier_init(k_height, k_width, n_features[2], n_features[2]), name="layer3/g23"),
            '2': tf.Variable(xavier_init(k_height, k_width, n_features[1], n_features[1]), name="layer2/g22"),
            '1': tf.Variable(xavier_init(k_height, k_width, n_features[0], n_features[0]), name="layer1/g21")}
        return all_weights

    def _auto_encoder(self, in_tensor, v_, w, w2, g, g2):

        v = tf.reshape(in_tensor, [width * height, -1])
        u = tf.reshape(in_tensor, [1, height, width, -1])
        h_ = tf.nn.relu(tf.nn.conv2d(u, w, [1, 1, 1, 1], "SAME"))
        h = tf.nn.relu(tf.nn.conv2d(h_, w2, [1, 1, 1, 1], "SAME"))
        out = self._generative_network(v_, h, w, w2, g, g2)

        # loss = tf.reduce_mean(tf.square(v - out))
        loss = tf.reduce_sum(tf.square(v - out))
        # loss = tf.reduce_mean(-tf.add(tf.mul(v, tf.log(1e-10 + out)), tf.mul(1 - v, tf.log(1e-10 + 1 - out))))

        hs = tf.reshape(h, [height, width, -1])
        return hs, loss, out

    def _generative_network(self, v_, hs, w, w2, g, g2):

        h = tf.reshape(hs, [1, height, width, -1])
        r_ = tf.nn.relu(tf.nn.conv2d(h, g2, [1, 1, 1, 1], "SAME"))
        r = tf.nn.conv2d(r_, g, [1, 1, 1, 1], "SAME")
        R = tf.reshape(r, [width * height, -1])
        out = tf.nn.relu(tf.matmul(R, v_, False))

        return out

    def _create_loss_optimizer(self):

        layer1_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer1")
        self.optimizer1 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss1, var_list=layer1_)

        layer2_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer2")
        self.optimizer2 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss2, var_list=layer2_)

        layer3_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer3")
        self.optimizer3 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss3, var_list=layer3_)

    def fine_tune_1(self, Y, v1_):
        opt, loss = self.sess.run((self.optimizer1, self.loss1), feed_dict={self.y: Y, self.v1_in: v1_})
        return loss

    def fine_tune_2(self, h1, v2_, Y, v1_):
        opt, loss = self.sess.run((self.optimizer2, self.loss2), feed_dict={self.h1: h1, self.v2_in: v2_, self.y: Y, self.v1_in: v1_})
        return loss

    def fine_tune_3(self, h2, v3_):
        opt, loss = self.sess.run((self.optimizer3, self.loss3), feed_dict={self.h2: h2, self.v3_in: v3_})
        return loss

    def get_content(self, Y):
        return self.sess.run((self.h1, self.h2, self.h3), feed_dict={self.y: Y})

    def get_content_1(self, Y):
        return self.sess.run(self.h1, feed_dict={self.y: Y})

    def get_content_2(self, h1):
        return self.sess.run(self.h2, feed_dict={self.h1: h1})

    def get_content_3(self, h2):
        return self.sess.run(self.h3, feed_dict={self.h2: h2})

    def transform(self, content, style):
        (h1, h2, h3) = self.get_content(content)
        (s1, s2, s3) = self.get_content(style)

        v1 = decompose(style)
        v2 = decompose(s1)
        v3 = decompose(s2)

        return self.sess.run(self.gen_output, feed_dict={self.v1_in: v1, self.v2_in: v2, self.v3_in: v3, self.h2_in: h2})

    def terminate(self):
        self.sess.close()

    def save1(self, file):
        self.saver1.save(self.sess, file)

    def save2(self, file):
        self.saver2.save(self.sess, file)

    def save3(self, file):
        self.saver3.save(self.sess, file)

    def load1(self, file):
        if os.path.isfile(file):
            self.saver1.restore(self.sess, file)
            return True
        else:
            return False

    def load2(self, file):
        if os.path.isfile(file):
            self.saver2.restore(self.sess, file)
            return True
        else:
            return False

    def load3(self, file):
        if os.path.isfile(file):
            self.saver3.restore(self.sess, file)
            return True
        else:
            return False


def decompose(input):
    A = np.reshape(input, (height * width, -1))
    (U, S, V) = np.linalg.svd(np.transpose(A).dot(A))
    return np.sqrt(np.diag(np.sqrt(S))).dot(V)


def compute_svd(data, out):
    for i in range(n_images):
        out[i, ...] = decompose(data[i, ...])


def train(network_architecture, learning_rate=0.001, batch_size=1, training_epochs=[10, 10, 10], display_step=1):
    network = Painter(network_architecture, learning_rate=learning_rate)

    svd_data1 = np.zeros((n_images, 3, 3), dtype='float32')
    compute_svd(dataset, svd_data1)
    if not network.load1("./decom/model1.weights") or (len(key_input) >= 1 and key_input[0] == 't'):
        print "training layer 1 ..."
        prev_loss = 0.
        for epoch in range(training_epochs[0]):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                batch_ys = dataset[i, ...]
                batch_vs = svd_data1[i, ...]
                loss = network.fine_tune_1(batch_ys, batch_vs)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save1("./decom/model1.weights")

    dataset2 = np.zeros((n_images, height, width, network_architecture['n_features'][0]), dtype='float32')
    for i in range(n_images):
        A = dataset[i, ...]
        dataset2[i, ...] = network.get_content_1(A)

    svd_data2 = np.zeros((n_images, network_architecture['n_features'][0], network_architecture['n_features'][0]), dtype='float32')
    compute_svd(dataset2, svd_data2)
    if not network.load2("./decom/model2.weights") or (len(key_input) >= 2 and key_input[1] == 't'):
        print "training layer 2 ..."
        prev_loss = 0.
        for epoch in range(training_epochs[1]):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                loss = network.fine_tune_2(dataset2[i, ...], svd_data2[i, ...], dataset[i, ...], svd_data1[i, ...])
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save2("./decom/model2.weights")

    dataset3 = np.zeros((n_images, height, width, network_architecture['n_features'][1]), dtype='float32')
    for i in range(n_images):
        A = dataset2[i, ...]
        dataset3[i, ...] = network.get_content_2(A)

    svd_data3 = np.zeros((n_images, network_architecture['n_features'][1], network_architecture['n_features'][1]), dtype='float32')
    compute_svd(dataset3, svd_data3)

    if not network.load3("./decom/model3.weights") or (len(key_input) >= 3 and key_input[2] == 't'):
        print "training layer 3 ..."
        prev_loss = 0.
        for epoch in range(training_epochs[2]):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                batch_ys = dataset3[i, ...]
                batch_vs = svd_data3[i, ...]
                loss = network.fine_tune_3(batch_ys, batch_vs)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), \
                    "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save3("./decom/model3.weights")

    return network

network_architecture = dict(n_features=[60, 60, 60],
                            k_width=7,
                            k_height=7)

painter = train(network_architecture, training_epochs=[1000, 1000, 1000])

pref_test = Image.open(prefix + '/pref_6.JPG')
pref_test.thumbnail((width, height), Image.ANTIALIAS)
style = np.asarray(pref_test) / 255.0

test = Image.open(prefix + '/raw_7.JPG')
test.thumbnail((width, height), Image.ANTIALIAS)
location = np.asarray(test) / 255.0

output_test = painter.transform(style, location)

plt.figure(1)
plt.imshow(location)

plt.figure(2)
plt.imshow(style)

plt.figure(3)
plt.imshow(np.minimum(np.maximum(output_test, 0.0), 1.0))

plt.show()
raw_input("enter")


painter.terminate()
