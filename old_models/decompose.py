import numpy as np
import tensorflow as tf
from PIL import Image
import os.path
import matplotlib.pyplot as plt


np.random.seed(0)
tf.set_random_seed(0)


def rgb2gray(rgb):
    return np.expand_dims(np.dot(rgb[..., :3], [0.299, 0.587, 0.114]), axis=2)


def invert(rgb):
    temp = rgb[..., 0]
    rgb[..., 0] = rgb[..., 2]
    rgb[..., 2] = temp


def divide(img, w, h, num):
    skip = 1
    rows = img.shape[0] / h
    cols = img.shape[1] / w
    data = np.empty([num, h, w, 3], dtype='float32')
    count = 0
    for i in range(1, rows, skip):
        for j in range(0, cols, skip):
            data[count, ...] = img[(i * h):((i + 1) * h), (j * w):((j + 1) * w), ...]
            count = count + 1
            if count >= num:
                return data
    return data

prefix = "./data"
n_images = 10
width = 120
height = 120

dataset = np.zeros((n_images, height, width, 3), dtype='float32')

index = 0
for i in range(1, 11, 1):
    img = Image.open(prefix + '/0' + str(i) + '.jpg')
    img = img.resize((width, height), Image.ANTIALIAS)
    dataset[index, ...] = np.asarray(img) / 255.0
    index = index + 1

plt.figure(1)
plt.imshow(dataset[4, ...])

plt.ion()
plt.show()
key_input = raw_input("enter")
print ">>>", len(key_input)


def xavier_init(h, w, fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((h, w, fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class Painter(object):

    def __init__(self, network_architecture, learning_rate=0.001):
        self.network_architecture = network_architecture
        self.learning_rate = learning_rate

        print network_architecture.viewkeys()
        self.y = tf.placeholder(tf.float32, [height, width, 3])

        self.h1_in = tf.placeholder(tf.float32, [height / 2, width / 2, network_architecture['n_features'][0]])
        self.h2_in = tf.placeholder(tf.float32, [height / 4, width / 4, network_architecture['n_features'][1]])
        self.h3_in = tf.placeholder(tf.float32, [height / 8, width / 8, network_architecture['n_features'][2]])

        self._create_network()
        self._create_loss_optimizer()

        init = tf.initialize_all_variables()

        layer1_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer1")
        self.saver1 = tf.train.Saver(var_list=layer1_)
        layer2_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer2")
        self.saver2 = tf.train.Saver(var_list=layer2_)
        layer3_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer3")
        self.saver3 = tf.train.Saver(var_list=layer3_)

        self.sess = tf.Session()
        self.sess.run(init)

    def _create_network(self):
        self.network_weights = self._initialize_weights(**self.network_architecture)
        (self.h1, self.loss1) = self._auto_encoder(height, width, self.y, self.network_weights['w']['1'], self.network_weights['w2']['1'], self.network_weights['g']['1'], self.network_weights['g2']['1'])
        self.ph1 = tf.reshape(self.h1, [1, height / 2, width / 2, -1])
        (self.h2, self.loss2) = self._auto_encoder(height / 2, width / 2, self.ph1, self.network_weights['w']['2'], self.network_weights['w2']['2'], self.network_weights['g']['2'], self.network_weights['g2']['2'])
        self.ph2 = tf.reshape(self.h2, [1, height / 4, width / 4, -1])
        (self.h3, self.loss3) = self._auto_encoder(height / 4, width / 4, self.ph2, self.network_weights['w']['3'], self.network_weights['w2']['3'], self.network_weights['g']['3'], self.network_weights['g2']['3'])

        self.h2_in_p = self._generative_network(height / 8, width / 8, self.h3_in, self.network_weights['w']['3'], self.network_weights['w2']['3'], self.network_weights['g']['3'], self.network_weights['g2']['3'])
        self.h2_in = tf.reshape(self.h2_in_p, [height / 4, width / 4, -1])
        self.h1_in_p = self._generative_network(height / 4, width / 4, self.h2_in, self.network_weights['w']['2'], self.network_weights['w2']['2'], self.network_weights['g']['2'], self.network_weights['g2']['2'])
        self.h1_in = tf.reshape(self.h1_in_p, [height / 2, width / 2, -1])
        self.h0_in_p = self._generative_network(height / 2, width / 2, self.h1_in, self.network_weights['w']['1'], self.network_weights['w2']['1'], self.network_weights['g']['1'], self.network_weights['g2']['1'])
        self.h0_in = tf.reshape(self.h0_in_p, [height, width, 3])

    def _initialize_weights(self, n_features, k_width, k_height):
        all_weights = dict()
        all_weights['w'] = {
            '1': tf.Variable(xavier_init(k_height, k_width, 3, n_features[0]), name="layer1/w1"),
            '2': tf.Variable(xavier_init(k_height, k_width, n_features[0], n_features[1]), name="layer2/w2"),
            '3': tf.Variable(xavier_init(k_height, k_width, n_features[1], n_features[2]), name="layer3/w3")}
        all_weights['w2'] = {
            '1': tf.Variable(xavier_init(k_height, k_width, n_features[0], n_features[0]), name="layer1/w21"),
            '2': tf.Variable(xavier_init(k_height, k_width, n_features[1], n_features[1]), name="layer2/w22"),
            '3': tf.Variable(xavier_init(k_height, k_width, n_features[2], n_features[2]), name="layer3/w23")}
        all_weights['g2'] = {
            '3': tf.Variable(xavier_init(k_height, k_width, n_features[2], n_features[2]), name="layer3/g23"),
            '2': tf.Variable(xavier_init(k_height, k_width, n_features[1], n_features[1]), name="layer2/g22"),
            '1': tf.Variable(xavier_init(k_height, k_width, n_features[0], n_features[0]), name="layer1/g21")}
        all_weights['g'] = {
            '3': tf.Variable(xavier_init(k_height, k_width, n_features[2], n_features[1]), name="layer3/g3"),
            '2': tf.Variable(xavier_init(k_height, k_width, n_features[1], n_features[0]), name="layer2/g2"),
            '1': tf.Variable(xavier_init(k_height, k_width, n_features[0], 3), name="layer1/g1")}
        return all_weights

    def _auto_encoder(self, height, width, in_tensor, w, w2, g, g2):

        v = tf.reshape(in_tensor, [1, height, width, -1])
        u = tf.nn.relu(tf.nn.conv2d(v, w, [1, 1, 1, 1], "SAME"))
        h = tf.nn.relu(tf.nn.conv2d(u, w2, [1, 1, 1, 1], "SAME"))
        pooled = tf.nn.max_pool(h, [1, 3, 3, 1], [1, 2, 2, 1], "SAME")
        v_ = self._generative_network(height / 2, width / 2, pooled, w, w2, g, g2)

        loss = tf.reduce_sum(tf.square(v - v_))
        hs = tf.reshape(pooled, [height / 2, width / 2, -1])
        return hs, loss

    def _generative_network(self, height, width, hs, w, w2, g, g2):

        h = tf.reshape(hs, [1, height, width, -1])
        p = tf.image.resize_images(h, [height * 2, width * 2])
        k = tf.nn.relu(tf.nn.conv2d(p, g2, [1, 1, 1, 1], "SAME"))
        v_ = tf.nn.relu(tf.nn.conv2d(k, g, [1, 1, 1, 1], "SAME"))
        return v_

    def _create_loss_optimizer(self):

        layer1_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer1")
        self.optimizer1 = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss1, var_list=layer1_)

        layer2_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer2")
        self.optimizer2 = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss2, var_list=layer2_)

        layer3_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer3")
        self.optimizer3 = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss3, var_list=layer3_)

    def fine_tune_1(self, Y):
        opt, loss = self.sess.run((self.optimizer1, self.loss1), feed_dict={self.y: Y})
        return loss

    def fine_tune_2(self, h1):
        opt, loss = self.sess.run((self.optimizer2, self.loss2), feed_dict={self.h1: h1})
        return loss

    def fine_tune_3(self, h2):
        opt, loss = self.sess.run((self.optimizer3, self.loss3), feed_dict={self.h2: h2})
        return loss

    def get_content(self, Y):
        return self.sess.run((self.h1, self.h2, self.h3), feed_dict={self.y: Y})

    def get_content_1(self, Y):
        return self.sess.run(self.h1, feed_dict={self.y: Y})

    def get_content_2(self, h1):
        return self.sess.run(self.h2, feed_dict={self.h1: h1})

    def get_content_3(self, h2):
        return self.sess.run(self.h3, feed_dict={self.h2: h2})

    def transform(self, content, style):
        (h1, h2, h3) = self.get_content(content)
        (s1, s2, s3) = self.get_content(style)

        h3_ = blend2(h3, h3, s3, width / 8, height / 8)
        h2e = self.sess.run(self.h2_in, feed_dict={self.h3_in: h3_})
        h2_ = blend2(h2e, h2e, s2, width / 4, height / 4)
        h1e = self.sess.run(self.h1_in, feed_dict={self.h2_in: h2_})
        h1_ = blend2(h1e, h1e, s1, width / 2, height / 2)
        h0e = self.sess.run(self.h0_in, feed_dict={self.h1_in: h1_})
        out = blend2(h0e, h0e, style, width, height)

        return out

    # def transform2(self, content, style):

    #     h0 = blend(content, style, width, height)

    #     ph1 = self.get_content_1(h0)
    #     s1 = self.get_content_1(style)
    #     h1 = blend(ph1, s1, width, height)

    #     ph1 = np.reshape(h1, (height / 2, width / 2, -1))
    #     ps1 = np.reshape(s1, (height / 2, width / 2, -1))

    #     ph2 = self.get_content_2(ph1)
    #     s2 = self.get_content_2(ps1)
    #     h2 = blend(ph2, s2, width / 2, height / 2)

    #     ph2 = np.reshape(h2, (height / 4, width / 4, -1))
    #     ps2 = np.reshape(s2, (height / 4, width / 4, -1))

    #     ph3 = self.get_content_3(ph2)
    #     s3 = self.get_content_3(ps2)
    #     h3 = blend(ph3, s3, width / 4, height / 4)

    #     return self.sess.run(self.gen_output, feed_dict={self.h3_in: h3})

    def terminate(self):
        self.sess.close()

    def save1(self, file):
        self.saver1.save(self.sess, file, write_meta_graph=False)

    def save2(self, file):
        self.saver2.save(self.sess, file, write_meta_graph=False)

    def save3(self, file):
        self.saver3.save(self.sess, file, write_meta_graph=False)

    def load1(self, file):
        if os.path.isfile(file + ".index"):
            self.saver1.restore(self.sess, file)
            return True
        else:
            return False

    def load2(self, file):
        if os.path.isfile(file + ".index"):
            self.saver2.restore(self.sess, file)
            return True
        else:
            return False

    def load3(self, file):
        if os.path.isfile(file + ".index"):
            self.saver3.restore(self.sess, file)
            return True
        else:
            return False


def blend(location, style, width, height):
    # find a rotation K such that R.K.v_s is closest to location and K'K = I (Frobenius norm)
    # R = location*(v_l)^(-1)
    v_s = decompose(style, width, height)
    v_l = decompose(location, width, height)

    v = np.reshape(location, (height * width, -1))
    R = v.dot(np.linalg.inv(v_l))

    # http://math.stackexchange.com/questions/207514/nonlinear-optimization-with-rotation-matrix-constraint
    (U, S, V) = np.linalg.svd(np.transpose(R).dot(v).dot(np.transpose(v_s)))
    K = U.dot(V)

    v_gen = R.dot(K.dot(v_s))
    return np.reshape(v_gen, (height, width, -1))


def blend2(embed, location, style, width, height):
    # Now that supposed we have embedded styles from higher layers and we want to embed another style in this layer.

    # find a rotation K such that R.K.v_s is closest to location and K'K = I (Frobenius norm)
    # R = embed*(v_e)^(-1)
    v_s = decompose(style, width, height)
    v_e = decompose(embed, width, height)

    v_L = np.reshape(location, (height * width, -1))
    v_E = np.reshape(embed, (height * width, -1))
    R = v_E.dot(np.linalg.inv(v_e))

    # http://math.stackexchange.com/questions/207514/nonlinear-optimization-with-rotation-matrix-constraint
    (U, S, V) = np.linalg.svd(np.transpose(R).dot(v_L).dot(np.transpose(v_s)))
    K = U.dot(V)

    v_gen = R.dot(K.dot(v_s))
    return np.reshape(v_gen, (height, width, -1))


def decompose(input, width, height):
    A = np.reshape(input, (height * width, -1))
    (U, S, V) = np.linalg.svd(np.transpose(A).dot(A))
    return np.sqrt(np.diag(np.sqrt(S))).dot(V)


def train(network_architecture, learning_rate=0.001, batch_size=1, training_epochs=[10, 10, 10], display_step=1):
    network = Painter(network_architecture, learning_rate=learning_rate)

    if not network.load1("./decom/model1.weights") or (len(key_input) >= 3 and key_input[2] == 't'):
        print "training layer 1 ..."
        prev_loss = 0.
        for epoch in range(training_epochs[0]):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                batch_ys = dataset[i, ...]
                loss = network.fine_tune_1(batch_ys)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save1("./decom/model1.weights")

    dataset2 = np.zeros((n_images, height / 2, width / 2, network_architecture['n_features'][0]), dtype='float32')
    for i in range(n_images):
        A = dataset[i, ...]
        dataset2[i, ...] = network.get_content_1(A)

    if not network.load2("./decom/model2.weights") or (len(key_input) >= 2 and key_input[1] == 't'):
        print "training layer 2 ..."
        prev_loss = 0.
        for epoch in range(training_epochs[1]):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                loss = network.fine_tune_2(dataset2[i, ...])
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save2("./decom/model2.weights")

    dataset3 = np.zeros((n_images, height / 4, width / 4, network_architecture['n_features'][1]), dtype='float32')
    for i in range(n_images):
        A = dataset2[i, ...]
        dataset3[i, ...] = network.get_content_2(A)

    if not network.load3("./decom/model3.weights") or (len(key_input) >= 1 and key_input[0] == 't'):
        print "training layer 3 ..."
        prev_loss = 0.
        for epoch in range(training_epochs[2]):
            avg_loss = 0.
            total_batch = n_images
            for i in range(total_batch):
                batch_ys = dataset3[i, ...]
                loss = network.fine_tune_3(batch_ys)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save3("./decom/model3.weights")

    return network


network_architecture = dict(n_features=[100, 200, 400],
                            k_width=3,
                            k_height=3)

painter = train(network_architecture, training_epochs=[1000, 1000, 1000])

pref_test = Image.open(prefix + '/04.jpg')
pref_test = pref_test.resize((width, height), Image.ANTIALIAS)
style = np.asarray(pref_test) / 255.0

test = Image.open(prefix + '/01.jpg')
test = test.resize((width, height), Image.ANTIALIAS)
location = np.asarray(test) / 255.0

output_test = painter.transform(location, style)
# output_test = blend(location, style, width, height)

plt.figure(1)
plt.imshow(location)

plt.figure(2)
plt.imshow(style)

plt.figure(3)
plt.imshow(np.minimum(np.maximum(output_test, 0.0), 1.0))

plt.show()
raw_input("enter")


# painter.terminate()
