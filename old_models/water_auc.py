import numpy as np
import tensorflow as tf
import Image

import matplotlib.pyplot as plt


np.random.seed(0)
tf.set_random_seed(0)

n_samples = 2
width = 160
height = 120

dataset = np.zeros((n_samples, height, width, 3))
label = np.zeros((n_samples, height, width, 3))


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])


prefix = "/home/tao/Desktop/WaterReplicator/data"
index = 0
for i in range(0, n_samples, 1):
    img = Image.open(prefix + '/raw_' + str(i) + '.JPG')
    img.thumbnail((width, height), Image.ANTIALIAS)
    dataset[index, ...] = np.asarray(img) / 255.0
    pref = Image.open(prefix + '/pref_' + str(i) + '.JPG')
    pref.thumbnail((width, height), Image.ANTIALIAS)
    label[index, ...] = np.asarray(pref) / 255.0
    index = index + 1

plt.figure(1)
plt.imshow(dataset[1, ...])

plt.figure(2)
plt.imshow(label[1, ...])

plt.show()


def xavier_init(h, w, fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    # https://stackoverflow.com/questions/33640581/how-to-do-xavier-initialization-on-tensorflow
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((h, w, fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class Painter(object):

    def __init__(self, network_architecture, transfer_fct=tf.nn.relu, learning_rate=0.001):
        self.network_architecture = network_architecture
        self.transfer_fct = transfer_fct
        self.learning_rate = learning_rate

        print network_architecture.viewkeys()
        # tf Graph input
        self.x = tf.placeholder(tf.float32, [height, width, 3])
        self.y = tf.placeholder(tf.float32, [height, width, 3])
        self.k_width = network_architecture["k_width"]
        self.k_height = network_architecture["k_height"]
        self.one = tf.ones([1, height, width, 1], tf.float32)

        self._create_network()
        self._create_loss_optimizer()

        # Initializing the tensor flow variables
        init = tf.initialize_all_variables()

        # Launch the session
        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self):
        # Initialize autoencode network weights and biases
        self.network_weights = self._initialize_weights(**self.network_architecture)
        self.xp, self.r = self._recognition_network(self.network_weights['recog'])
        self.yp = self._generative_network(self.network_weights['gen'])

    def get_weights(self):
        return self.sess.run((
            self.network_weights['recog']['h1'],
            self.network_weights['recog']['h2'],
            self.network_weights['recog']['h3'],
            self.network_weights['recog']['h4'],
            self.network_weights['recog']['h5'],
            self.network_weights['recog']['out'],
            self.network_weights['recog']['b1'],
            self.network_weights['recog']['b2'],
            self.network_weights['recog']['b3'],
            self.network_weights['recog']['b4'],
            self.network_weights['recog']['b5'],
            self.network_weights['recog']['b_out'],
            self.network_weights['gen']['h1'],
            self.network_weights['gen']['h2'],
            self.network_weights['gen']['h3'],
            self.network_weights['gen']['h4'],
            self.network_weights['gen']['h5'],
            self.network_weights['gen']['out'],
            self.network_weights['gen']['b1'],
            self.network_weights['gen']['b2'],
            self.network_weights['gen']['b3'],
            self.network_weights['gen']['b4'],
            self.network_weights['gen']['b5'],
            self.network_weights['gen']['b_out']))

    def get_residue(self, Y):
        return self.sess.run(self.r, feed_dict={self.y: Y})

    def _initialize_weights(self, n_hidden_recog_1, n_hidden_recog_2, n_hidden_recog_3, n_hidden_recog_4, n_hidden_recog_5, k_width, k_height):
        all_weights = dict()
        all_weights['recog'] = {
            'h1': tf.Variable(xavier_init(k_height, k_width, 3, n_hidden_recog_1)),
            'h2': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_1, n_hidden_recog_2)),
            'h3': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_2, n_hidden_recog_3)),
            'h4': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_3, n_hidden_recog_4)),
            'h5': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_4, n_hidden_recog_5)),
            'out': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_5, 3)),
            'b1': tf.Variable(xavier_init(k_height, k_width, 1, 3)),
            'b2': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_1)),
            'b3': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_2)),
            'b4': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_3)),
            'b5': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_4)),
            'b_out': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_5))}
        all_weights['gen'] = {
            'h1': tf.Variable(xavier_init(k_height, k_width, 3, n_hidden_recog_1)),
            'h2': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_1, n_hidden_recog_2)),
            'h3': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_2, n_hidden_recog_3)),
            'h4': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_3, n_hidden_recog_4)),
            'h5': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_4, n_hidden_recog_5)),
            'out': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_5, 3)),
            'b1': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_1)),
            'b2': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_2)),
            'b3': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_3)),
            'b4': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_4)),
            'b5': tf.Variable(xavier_init(k_height, k_width, 1, n_hidden_recog_5)),
            'b_out': tf.Variable(xavier_init(k_height, k_width, 1, 3))}
        return all_weights

    def _recognition_network(self, weights):

        input4d = tf.reshape(self.y, [1, height, width, 3])

        layer_1 = self.transfer_fct(tf.nn.conv2d(input4d, weights["h1"], [1, 1, 1, 1], "SAME"))
        layer_2 = self.transfer_fct(tf.nn.conv2d(layer_1, weights["h2"], [1, 1, 1, 1], "SAME"))
        layer_3 = self.transfer_fct(tf.nn.conv2d(layer_2, weights["h3"], [1, 1, 1, 1], "SAME"))
        layer_4 = self.transfer_fct(tf.nn.conv2d(layer_3, weights["h4"], [1, 1, 1, 1], "SAME"))
        layer_5 = self.transfer_fct(tf.nn.conv2d(layer_4, weights["h5"], [1, 1, 1, 1], "SAME"))
        x_reconstr_mean = tf.nn.sigmoid(tf.nn.conv2d(layer_5, weights["out"], [1, 1, 1, 1], "SAME"))
        out = tf.reshape(x_reconstr_mean, [height, width, 3])

        # r = self.transfer_fct(
        #     tf.add(
        #         tf.add(
        #             tf.add(
        #                 tf.nn.conv2d(input4d, weights["b1"], [1, 1, 1, 1], "SAME"),
        #                 tf.nn.conv2d(layer_1, weights["b2"], [1, 1, 1, 1], "SAME")),
        #             tf.add(
        #                 tf.nn.conv2d(layer_2, weights["b3"], [1, 1, 1, 1], "SAME"),
        #                 tf.nn.conv2d(layer_3, weights["b4"], [1, 1, 1, 1], "SAME"))),
        #         tf.add(
        #             tf.nn.conv2d(layer_4, weights["b5"], [1, 1, 1, 1], "SAME"),
        #             tf.nn.conv2d(layer_5, weights["b_out"], [1, 1, 1, 1], "SAME"))))

        r = self.transfer_fct(tf.add(
            tf.nn.conv2d(layer_4, weights["b5"], [1, 1, 1, 1], "SAME"),
            tf.nn.conv2d(layer_5, weights["b_out"], [1, 1, 1, 1], "SAME")))

        return out, r

    def _generative_network(self, weights):

        input4d = tf.reshape(self.xp, [1, height, width, 3])

        layer_1 = self.transfer_fct(tf.add(
            tf.nn.conv2d(input4d, weights["h1"], [1, 1, 1, 1], "SAME"),
            tf.nn.conv2d(self.r, weights["b1"], [1, 1, 1, 1], "SAME")))
        layer_2 = self.transfer_fct(tf.add(
            tf.nn.conv2d(layer_1, weights["h2"], [1, 1, 1, 1], "SAME"),
            tf.nn.conv2d(self.r, weights["b2"], [1, 1, 1, 1], "SAME")))
        layer_3 = self.transfer_fct(tf.nn.conv2d(layer_2, weights["h3"], [1, 1, 1, 1], "SAME"))
        layer_4 = self.transfer_fct(tf.nn.conv2d(layer_3, weights["h4"], [1, 1, 1, 1], "SAME"))
        layer_5 = self.transfer_fct(tf.nn.conv2d(layer_4, weights["h5"], [1, 1, 1, 1], "SAME"))
        x_reconstr_mean = tf.nn.sigmoid(tf.nn.conv2d(layer_5, weights["out"], [1, 1, 1, 1], "SAME"))

        out = tf.reshape(x_reconstr_mean, [height, width, 3])

        return out

    def _create_loss_optimizer(self):
        error1 = -tf.add(tf.mul(self.y, tf.log(1e-10 + self.yp)), tf.mul(1 - self.y, tf.log(1e-10 + 1 - self.yp)))
        error2 = -tf.add(tf.mul(self.x, tf.log(1e-10 + self.xp)), tf.mul(1 - self.x, tf.log(1e-10 + 1 - self.xp)))
        self.loss = tf.add(tf.reduce_mean(error1, name='xentropy_mean'), tf.reduce_mean(error2, name='xentropy_mean'))

        self.optimizer = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss)

    def partial_fit(self, X, Y):
        """Train model based on mini-batch of input data.

        Return loss of mini-batch.
        """
        opt, loss = self.sess.run((self.optimizer, self.loss), feed_dict={self.x: X, self.y: Y})
        return loss

    def transform(self, X, R):
        """Transform data by mapping it into the latent space."""
        # Note: This maps to mean of distribution, we could alternatively
        # sample from Gaussian distribution
        return self.sess.run(self.yp, feed_dict={self.xp: X, self.r: R})

    def terminate(self):
        self.sess.close()


def train(network_architecture, learning_rate=0.001, batch_size=1, training_epochs=10, display_step=10):
    network = Painter(network_architecture, learning_rate=learning_rate)

    # Training cycle
    prev_loss = 0.
    for epoch in range(training_epochs):
        avg_loss = 0.
        total_batch = n_samples
        # Loop over all batches
        for i in range(total_batch):
            batch_xs = dataset[i, ...]
            batch_ys = label[i, ...]

            # Fit training using batch data
            loss = network.partial_fit(batch_xs, batch_ys)
            # Compute average loss
            avg_loss += loss / n_samples * batch_size

        # Display logs per epoch step
        if epoch % display_step == 0:
            print "Epoch:", '%04d' % (epoch + 1), \
                "loss=", "{:.9f}".format(avg_loss)

        if abs(avg_loss - prev_loss) < 1e-8:
            break

        prev_loss = avg_loss

    return network


network_architecture = \
    dict(n_hidden_recog_1=30,  # 1st layer encoder neurons
         n_hidden_recog_2=30,  # 2nd layer encoder neurons
         n_hidden_recog_3=30,  # 2nd layer encoder neurons
         n_hidden_recog_4=30,  # 2nd layer encoder neurons
         n_hidden_recog_5=30,  # 2nd layer encoder neurons
         k_width=15,
         k_height=15)

painter = train(network_architecture, training_epochs=30000)
# painter = Painter(network_architecture, learning_rate=0.1)

(h1, h2, h3, h4, h5, out, b1, b2, b3, b4, b5, b_out,
 g_h1, g_h2, g_h3, g_h4, g_h5, g_out, g_b1, g_b2, g_b3, g_b4, g_b5, g_b_out) = painter.get_weights()
# print h1, h2, out

np.save("./h1.mat", h1)
np.save("./h2.mat", h2)
np.save("./h3.mat", h3)
np.save("./h4.mat", h4)
np.save("./h5.mat", h5)
np.save("./out.mat", out)
np.save("./b1.mat", b1)
np.save("./b2.mat", b2)
np.save("./b3.mat", b3)
np.save("./b4.mat", b4)
np.save("./b5.mat", b5)
np.save("./b_out.mat", b_out)

np.save("./g_h1.mat", g_h1)
np.save("./g_h2.mat", g_h2)
np.save("./g_h3.mat", g_h3)
np.save("./g_h4.mat", g_h4)
np.save("./g_h5.mat", g_h5)
np.save("./g_out.mat", g_out)
np.save("./g_b1.mat", g_b1)
np.save("./g_b2.mat", g_b2)
np.save("./g_b3.mat", g_b3)
np.save("./g_b4.mat", g_b4)
np.save("./g_b5.mat", g_b5)
np.save("./g_b_out.mat", g_b_out)


pref_test = Image.open(prefix + '/pref_0.JPG')
pref_test.thumbnail((width, height), Image.ANTIALIAS)
img_pref = np.asarray(pref_test) / 255.0
img_residue = painter.get_residue(img_pref)

test = Image.open(prefix + '/raw_7.JPG')
test.thumbnail((width, height), Image.ANTIALIAS)
img_test = np.asarray(test) / 255.0

output_test = painter.transform(img_test, img_residue)


plt.figure(1)
plt.imshow(img_test)

plt.figure(2)
plt.imshow(pref_test)

plt.figure(3)
plt.imshow(output_test)
plt.show()

painter.terminate()
