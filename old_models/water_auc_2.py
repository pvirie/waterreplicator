import numpy as np
import tensorflow as tf
import Image
import os.path

import matplotlib.pyplot as plt


np.random.seed(0)
tf.set_random_seed(0)

n_samples = 40
width = 100
height = 75


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])


def divide(img, w, h, num):
    skip = 2
    rows = img.shape[0] / h
    cols = img.shape[1] / w
    data = np.empty([num, h, w, 3], dtype=float)
    count = 0
    for i in range(1, rows, skip):
        for j in range(0, cols, skip):
            data[count, ...] = img[(i * h):((i + 1) * h), (j * w):((j + 1) * w), ...]
            count = count + 1
            if count >= num:
                return data
    return data

dataset = np.zeros((n_samples, height, width, 3), dtype=float)
label = np.zeros((n_samples, height, width, 3), dtype=float)

prefix = "/home/tao/Desktop/WaterReplicator/data"
n_images = 2
n_sample_per_image = 20

index = 0
for i in range(0, n_images, 1):
    img = Image.open(prefix + '/raw_' + str(i) + '.JPG')
    pref = Image.open(prefix + '/pref_' + str(i) + '.JPG')
    dataset[index:(index + n_sample_per_image), ...] = divide(np.asarray(img) / 255.0, width, height, n_sample_per_image)
    label[index:(index + n_sample_per_image), ...] = divide(np.asarray(pref) / 255.0, width, height, n_sample_per_image)
    index = index + n_sample_per_image

plt.figure(1)
plt.imshow(dataset[7, ...])
plt.figure(2)
plt.imshow(label[7, ...])

plt.ion()
plt.show()
raw_input("enter")


def xavier_init(h, w, fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    # https://stackoverflow.com/questions/33640581/how-to-do-xavier-initialization-on-tensorflow
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((h, w, fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class Painter(object):

    def __init__(self, network_architecture, transfer_fct=tf.nn.relu, learning_rate=0.001):
        self.network_architecture = network_architecture
        self.transfer_fct = transfer_fct
        self.learning_rate = learning_rate

        print network_architecture.viewkeys()
        self.x = tf.placeholder(tf.float32, [height, width, 3])
        self.y = tf.placeholder(tf.float32, [height, width, 3])
        self.k_width = network_architecture["k_width"]
        self.k_height = network_architecture["k_height"]
        self.one = tf.ones([1, height, width, 1], tf.float32)

        self._create_network()
        self._create_loss_optimizer()

        init = tf.initialize_all_variables()
        self.saver = tf.train.Saver()

        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self):
        self.network_weights = self._initialize_weights(**self.network_architecture)
        self.yp = self._auto_encoder(self.network_weights['recog'], self.network_weights['gen'])
        self.yc = self._generative_network(self.network_weights['gen'])

    def get_weights(self):
        return self.sess.run((
            self.network_weights['recog']['h1'],
            self.network_weights['recog']['h2'],
            self.network_weights['recog']['h3'],
            self.network_weights['recog']['out'],
            self.network_weights['recog']['b1'],
            self.network_weights['recog']['b2'],
            self.network_weights['recog']['b3'],
            self.network_weights['recog']['b_out'],
            self.network_weights['gen']['h1'],
            self.network_weights['gen']['h2'],
            self.network_weights['gen']['h3'],
            self.network_weights['gen']['out'],
            self.network_weights['gen']['b1'],
            self.network_weights['gen']['b2'],
            self.network_weights['gen']['b3'],
            self.network_weights['gen']['b_out']))

    def get_residue(self, Y):
        return self.sess.run(self.r, feed_dict={self.y: Y})

    def _initialize_weights(self, n_hidden_recog_1, n_hidden_recog_2, n_hidden_recog_3, k_width, k_height):
        all_weights = dict()
        all_weights['recog'] = {
            'h1': tf.Variable(xavier_init(k_height, k_width, 3, n_hidden_recog_1), name="second_phase/r_h1"),
            'h2': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_1, n_hidden_recog_2), name="second_phase/r_h2"),
            'h3': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_2, n_hidden_recog_3), name="second_phase/r_h3"),
            'out': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_3, 3), name="second_phase/r_out"),
            'b1': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_1, 3), name="second_phase/r_b1"),
            'b2': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_2, 3), name="second_phase/r_b2"),
            'b3': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_3, 3), name="second_phase/r_b3"),
            'b_out': tf.Variable(xavier_init(k_height, k_width, 3, 3), name="second_phase/r_bout")}
        all_weights['gen'] = {
            'h1': tf.Variable(xavier_init(k_height, k_width, 3, n_hidden_recog_1), name="first_phase/g_h1"),
            'h2': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_1, n_hidden_recog_2), name="first_phase/g_h2"),
            'h3': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_2, n_hidden_recog_3), name="first_phase/g_h3"),
            'out': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_3, 3), name="first_phase/g_out"),
            'b1': tf.Variable(xavier_init(k_height, k_width, 3, n_hidden_recog_1), name="second_phase/g_b1"),
            'b2': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_1, n_hidden_recog_2), name="second_phase/g_b2"),
            'b3': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_2, n_hidden_recog_3), name="second_phase/g_b3"),
            'b_out': tf.Variable(xavier_init(k_height, k_width, n_hidden_recog_3, 3), name="second_phase/g_bout")}
        return all_weights

    def _auto_encoder(self, recog, gen):

        inputY = tf.reshape(self.y, [1, height, width, 3])

        r_1 = self.transfer_fct(tf.nn.conv2d(inputY, recog["h1"], [1, 1, 1, 1], "SAME"))
        r_2 = self.transfer_fct(tf.nn.conv2d(r_1, recog["h2"], [1, 1, 1, 1], "SAME"))
        r_3 = self.transfer_fct(tf.nn.conv2d(r_2, recog["h3"], [1, 1, 1, 1], "SAME"))
        self.r = self.transfer_fct(tf.nn.conv2d(r_3, recog["out"], [1, 1, 1, 1], "SAME"))

        inputX = tf.reshape(self.x, [1, height, width, 3])

        g_1 = self.transfer_fct(tf.add(
            tf.nn.conv2d(self.r, gen["b1"], [1, 1, 1, 1], "SAME"),
            tf.nn.conv2d(inputX, gen["h1"], [1, 1, 1, 1], "SAME")))
        g_2 = self.transfer_fct(
            tf.nn.conv2d(g_1, gen["h2"], [1, 1, 1, 1], "SAME"))
        g_3 = self.transfer_fct(
            tf.nn.conv2d(g_2, gen["h3"], [1, 1, 1, 1], "SAME"))

        yp_ = tf.nn.sigmoid(
            tf.nn.conv2d(g_3, gen["out"], [1, 1, 1, 1], "SAME"))

        out = tf.reshape(yp_, [height, width, 3])

        return out

    def _generative_network(self, weights):

        inputX = tf.reshape(self.x, [1, height, width, 3])

        layer_1 = self.transfer_fct(tf.nn.conv2d(inputX, weights["h1"], [1, 1, 1, 1], "SAME"))
        layer_2 = self.transfer_fct(tf.nn.conv2d(layer_1, weights["h2"], [1, 1, 1, 1], "SAME"))
        layer_3 = self.transfer_fct(tf.nn.conv2d(layer_2, weights["h3"], [1, 1, 1, 1], "SAME"))

        yc_ = tf.nn.sigmoid(tf.nn.conv2d(layer_3, weights["out"], [1, 1, 1, 1], "SAME"))

        out = tf.reshape(yc_, [height, width, 3])

        return out

    def _create_loss_optimizer(self):

        error2 = -tf.add(tf.mul(self.y, tf.log(1e-10 + self.yc)), tf.mul(1 - self.y, tf.log(1e-10 + 1 - self.yc)))
        self.loss2 = tf.reduce_mean(error2)
        self.optimizer2 = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss2)

        train_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "second_phase")
        error1 = -tf.add(tf.mul(self.y, tf.log(1e-10 + self.yp)), tf.mul(1 - self.y, tf.log(1e-10 + 1 - self.yp)))
        self.loss = tf.reduce_mean(error1)
        self.optimizer = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss, var_list=train_vars)

    def partial_fit(self, X, Y):
        opt, loss = self.sess.run((self.optimizer2, self.loss2), feed_dict={self.x: X, self.y: Y})
        return loss

    def fine_tune(self, X, Y):
        opt, loss = self.sess.run((self.optimizer, self.loss), feed_dict={self.x: X, self.y: Y})
        return loss

    def transform(self, X, Y):
        return self.sess.run(self.yp, feed_dict={self.x: X, self.y: Y})

    def transform_no_residue(self, X):
        return self.sess.run(self.yc, feed_dict={self.x: X})

    def terminate(self):
        self.sess.close()

    def save(self, file):
        self.saver.save(self.sess, file)

    def load(self, file):
        if os.path.isfile(file):
            self.saver.restore(self.sess, file)
            return True
        else:
            return False


def train(network_architecture, learning_rate=0.002, batch_size=1, training_epochs=10, display_step=10):
    network = Painter(network_architecture, learning_rate=learning_rate)

    if network.load("./model.weights"):
        return network

    prev_loss = 0.
    for epoch in range(training_epochs):
        avg_loss = 0.
        total_batch = n_samples
        for i in range(total_batch):
            batch_xs = dataset[i, ...]
            batch_ys = label[i, ...]
            loss = network.partial_fit(batch_xs, batch_ys)
            avg_loss += loss / n_samples

        if epoch % display_step == 0:
            print "Epoch:", '%04d' % (epoch + 1), \
                "loss=", "{:.9f}".format(avg_loss)

        if abs(avg_loss - prev_loss) < 1e-8:
            break

        prev_loss = avg_loss

    print "FINE TUNING..."

    prev_loss = 0.
    for epoch in range(training_epochs):
        avg_loss = 0.
        total_batch = n_samples
        for i in range(total_batch):
            batch_xs = dataset[i, ...]
            batch_ys = label[i, ...]
            loss = network.fine_tune(batch_xs, batch_ys)
            avg_loss += loss / n_samples

        if epoch % display_step == 0:
            print "Epoch:", '%04d' % (epoch + 1), \
                "loss=", "{:.9f}".format(avg_loss)

        if abs(avg_loss - prev_loss) < 1e-8:
            break

        prev_loss = avg_loss

    network.save("./model.weights")

    return network


network_architecture = \
    dict(n_hidden_recog_1=600,  # 1st layer encoder neurons
         n_hidden_recog_2=600,  # 2nd layer encoder neurons
         n_hidden_recog_3=600,  # 2nd layer encoder neurons
         k_width=7,
         k_height=7)

painter = train(network_architecture, training_epochs=20000)

pref_test = Image.open(prefix + '/pref_3.JPG')
img_pref = np.asarray(pref_test) / 255.0

test = Image.open(prefix + '/raw_7.JPG')
img_test = np.asarray(test) / 255.0

num_show = 19

location = divide(img_test, width, height, num_show)
style = divide(img_pref, width, height, num_show)

for i in range(num_show):
    output_test = painter.transform(location[i, ...], style[i, ...])
    output_test_no_residue = painter.transform_no_residue(location[i, ...])

    plt.figure(1)
    plt.imshow(location[i, ...])

    plt.figure(2)
    plt.imshow(style[i, ...])

    plt.figure(3)
    plt.imshow(output_test)

    plt.figure(4)
    plt.imshow(output_test_no_residue)

    plt.show()
    raw_input("enter")

painter.terminate()
