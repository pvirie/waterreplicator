import numpy as np
import tensorflow as tf
from PIL import Image
import os.path
import matplotlib.pyplot as plt
import math


# np.random.seed()
# tf.set_random_seed()


def rgb2gray(rgb):
    return np.expand_dims(np.dot(rgb[..., :3], [0.299, 0.587, 0.114]), axis=2)


def invert(rgb):
    temp = rgb[..., 0]
    rgb[..., 0] = rgb[..., 2]
    rgb[..., 2] = temp


def divide(img, w, h, num):
    skip = 1
    rows = img.shape[0] / h
    cols = img.shape[1] / w
    data = np.empty([num, h, w, 3], dtype='float32')
    count = 0
    for i in range(1, rows, skip):
        for j in range(0, cols, skip):
            data[count, ...] = img[(i * h):((i + 1) * h), (j * w):((j + 1) * w), ...]
            count = count + 1
            if count >= num:
                return data
    return data

prefix = "./data"
n_images = 10
width = 120
height = 120

dataset = np.zeros((n_images, height, width, 3), dtype='float32')

index = 0
for i in range(1, 11, 1):
    img = Image.open(prefix + '/0' + str(i) + '.jpg')
    img = img.resize((width, height), Image.ANTIALIAS)
    dataset[index, ...] = np.asarray(img) / 255.0
    index = index + 1

plt.figure(1)
plt.imshow(dataset[4, ...])

plt.ion()
plt.show()
key_input = raw_input("enter")
print ">>>", len(key_input)


def xavier_init(h, w, fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((h, w, fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)

sess = tf.Session()


class ConvLayer(object):

    def __init__(self, network_architecture, use_pooling=True, learning_rate=0.001, path="./weights/w.conv"):
        self.network_architecture = network_architecture
        self.learning_rate = learning_rate
        self.path = path
        self.use_pooling = use_pooling

        print network_architecture.viewkeys()
        self.y = tf.placeholder(tf.float32, [None, None, None, None])
        self.x = tf.placeholder(tf.float32, [None, None, None, None])

        self._create_network()
        self._create_loss_optimizer()

        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer")
        self.saver = tf.train.Saver(var_list=layer_)

    def _create_network(self):
        self.network_weights = self._initialize_weights(**self.network_architecture)
        (self.h, self.v_, self.loss) = self._auto_encoder(self.y, self.network_weights['w'], self.network_weights['w2'], self.network_weights['g'], self.network_weights['g2'])

        if self.use_pooling:
            shape = tf.shape(self.x)
            temp = tf.image.resize_images(self.x, [shape[1] * 2, shape[2] * 2])
        else:
            temp = self.x

        self.u = self._generative(temp, self.network_weights['g'], self.network_weights['g2'])

    def _initialize_weights(self, in_features, out_features, k_width, k_height):
        all_weights = dict()
        all_weights['w'] = tf.Variable(xavier_init(k_height, k_width, in_features, out_features), name="layer/w1")
        all_weights['w2'] = tf.Variable(xavier_init(k_height, k_width, out_features, out_features), name="layer/w21")
        all_weights['g2'] = tf.Variable(xavier_init(k_height, k_width, out_features, out_features), name="layer/g23")
        all_weights['g'] = tf.Variable(xavier_init(k_height, k_width, out_features, in_features), name="layer/g3")
        return all_weights

    def _auto_encoder(self, in_tensor, w, w2, g, g2):
        shape = tf.shape(in_tensor)
        v = tf.reshape(in_tensor, [shape[0], shape[1], shape[2], -1])
        u = tf.nn.relu(tf.nn.conv2d(v, w, [1, 1, 1, 1], "SAME"))
        h = tf.nn.relu(tf.nn.conv2d(u, w2, [1, 1, 1, 1], "SAME"))

        if self.use_pooling:
            h, h_ = self._pooling_layer(h)
            v_ = self._generative(h_, g, g2)
        else:
            v_ = self._generative(h, g, g2)

        loss = tf.reduce_sum(tf.square(v - v_))
        return h, v_, loss

    def _pooling_layer(self, in_tensor):
        shape = tf.shape(in_tensor)
        h = tf.nn.max_pool(in_tensor, [1, 3, 3, 1], [1, 2, 2, 1], "SAME")
        h_ = tf.image.resize_images(h, [shape[1], shape[2]])
        return h, h_

    def _generative(self, in_tensor, g, g2):
        k = tf.nn.relu(tf.nn.conv2d(in_tensor, g2, [1, 1, 1, 1], "SAME"))
        return tf.nn.relu(tf.nn.conv2d(k, g, [1, 1, 1, 1], "SAME"))

    def _create_loss_optimizer(self):
        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "layer")
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss, var_list=layer_)

    def train(self, Y):
        opt, loss = sess.run((self.optimizer, self.loss), feed_dict={self.y: Y})
        return loss

    def get_embedding(self, Y):
        return sess.run(self.h, feed_dict={self.y: Y})

    def get_generated(self, X):
        return sess.run(self.u, feed_dict={self.x: X})

    def terminate(self):
        sess.close()

    def save(self):
        self.saver.save(sess, self.path, write_meta_graph=False)

    def load(self):
        print "loading..", self.path
        if os.path.isfile(self.path + ".index"):
            self.saver.restore(sess, self.path)
            return True
        else:
            return False


def blend(location, style):
    # find a rotation K such that R.K.v_s is closest to location and K'K = I (Frobenius norm)
    # R = location*(v_l)^(-1)
    v_s = decompose(style)
    v_l = decompose(location)

    width = location.shape[1]
    height = location.shape[0]
    v = np.reshape(location, (height * width, -1))
    R = v.dot(np.linalg.inv(v_l))

    # http://math.stackexchange.com/questions/207514/nonlinear-optimization-with-rotation-matrix-constraint
    (U, S, V) = np.linalg.svd(np.transpose(R).dot(v).dot(np.transpose(v_s)))
    K = U.dot(V)

    v_gen = R.dot(K.dot(v_s))
    return np.reshape(v_gen, (height, width, -1))


def blend2(embed, location, style):
    # Now that supposed we have embedded styles from higher layers and we want to embed another style in this layer.

    # find a rotation K such that R.K.v_s is closest to embed and K'K = I (Frobenius norm)
    # R = embed*(v_e)^(-1)
    v_s = decompose(style)
    v_l = decompose(location)

    width = location.shape[1]
    height = location.shape[0]
    v_L = np.reshape(location, (height * width, -1))
    v_E = np.reshape(embed, (embed.shape[0] * embed.shape[1], -1))
    R = v_L.dot(np.linalg.inv(v_l))

    # http://math.stackexchange.com/questions/207514/nonlinear-optimization-with-rotation-matrix-constraint
    (U, S, V) = np.linalg.svd(np.transpose(R).dot(v_E).dot(np.transpose(v_s)))
    K = U.dot(V)

    v_gen = R.dot(K.dot(v_s))
    return np.reshape(v_gen, (height, width, -1))


def decompose(input):
    width = input.shape[1]
    height = input.shape[0]
    A = np.reshape(input, (height * width, -1))
    (U, S, V) = np.linalg.svd(np.transpose(A).dot(A))
    return np.sqrt(np.diag(np.sqrt(S))).dot(V)


def train(learning_rate=0.001, batch_size=1, display_step=1):

    layers = []
    training_epochs = [500, 500, 500, 1000]
    use_pooling = [True, False, True, True]
    features = [3, 100, 100, 200, 400]
    D = dataset

    for i in xrange(len(training_epochs)):
        spec = dict(in_features=features[i], out_features=features[i + 1], k_width=3, k_height=3)
        layer = ConvLayer(spec, use_pooling=use_pooling[i], learning_rate=learning_rate, path="./weights/model" + str(i) + ".weights")
        layers.append(layer)
        print spec

    init = tf.global_variables_initializer()
    sess.run(init)

    for i in xrange(len(training_epochs)):
        layer = layers[i]
        if ((len(key_input) >= len(training_epochs) - i) and (key_input[len(training_epochs) - 1 - i] == 't')) or not layer.load():
            print "training layer ", i
            prev_loss = 0.
            for epoch in xrange(training_epochs[i]):
                avg_loss = 0.
                total_batch = n_images / batch_size
                for i in range(total_batch):
                    batch_ys = D[i:i + batch_size, ...]
                    loss = layer.train(batch_ys)
                    avg_loss += loss / total_batch

                if epoch % display_step == 0:
                    print "Epoch:", '%04d' % (epoch + 1), "loss=", "{:.9f}".format(avg_loss)

                if abs(avg_loss - prev_loss) < 1e-8:
                    break

                prev_loss = avg_loss
            layer.save()
        D = layer.get_embedding(D)

    return layers


def to3dim(x):
    return np.reshape(x, (x.shape[1], x.shape[2], x.shape[3]))


def to4dim(x):
    return np.reshape(x, (1, x.shape[0], x.shape[1], x.shape[2]))


def transform(layers, location, style):

    l = location
    el = []
    s = style
    es = []
    for i in xrange(len(layers)):
        l = to3dim(layers[i].get_embedding(to4dim(l)))
        el.append(l)
        s = to3dim(layers[i].get_embedding(to4dim(s)))
        es.append(s)

    # the top most layer we blend style to match location L*(S_l)^-1*(S_s)
    # lower layers, we blend the style from upper layer with current layer's

    b = l
    for i in xrange(len(layers) - 1, -1, -1):
        print b.shape, el[i].shape, es[i].shape
        if i >= len(layers) - 1:
            t = el[i]
        else:
            t = b
        b = to3dim(layers[i].get_generated(to4dim(blend2(b, t, es[i]))))

    # b = to3dim(layers[0].get_generated(layers[0].get_embedding(to4dim(location))))
    return np.minimum(np.maximum(b, 0.0), 1.0)


def make_tile(mats, rows, cols, flip=False):
    b = len(mats)
    if b <= 0:
        return None
    r = mats[0].shape[1] if flip else mats[0].shape[0]
    c = mats[0].shape[0] if flip else mats[0].shape[1]
    canvas = np.zeros((rows, cols, 3 if len(mats[0].shape) > 2 else 1), dtype=mats[0].dtype)
    step = int(max(1, math.floor(b * (r * c) / (rows * cols))))
    i = 0
    for x in xrange(int(math.floor(rows / r))):
        for y in xrange(int(math.floor(cols / c))):
            canvas[(x * r):((x + 1) * r), (y * c):((y + 1) * c), :] = np.transpose(mats[i], (1, 0, 2)) if flip else mats[i]
            i = (i + step) % b

    return canvas


layers = train()

imgs = []

for i in xrange(6):

    test = Image.open(prefix + '/0' + str(np.random.randint(1, 10)) + '.jpg')
    test = test.resize((width, height), Image.ANTIALIAS)
    location = np.asarray(test) / 255.0
    imgs.append(location)

    pref_test = Image.open(prefix + '/0' + str(np.random.randint(1, 10)) + '.jpg')
    pref_test = pref_test.resize((width, height), Image.ANTIALIAS)
    style = np.asarray(pref_test) / 255.0
    imgs.append(style)

    output_test = transform(layers, location, style)
    imgs.append(output_test)


plt.figure(1)
plt.imshow(make_tile(imgs, 360, 120 * 6))

plt.show()
raw_input("enter")
