# Harmoniously Information Blending #

![Demo](figure_1.png)

The content of most information spans across the low frequency spectrum, while the style of the information are those reside within high frequency.
Convolutional pooling helps us extract low frequency information at the deeper layers while the intermediate layers present higher frequencies.
Content = low frequency
Style = high frequency

Combining more than one information such that the result satisfies the "internal consistency".

* At each layer, given an image L and a target style image S,
* L^T L can be decomposed as v^T Rl^T Rl v = v^T v ~ vs^T R^T R vs = (Rl.K.vs)^T Rl.K.vs
* where S^T S = vs^T R'^T R' vs = vs^T vs
* find a rotation K such that Rl.K.vs is closest to location and K'K = I (Frobenius norm)
* min tr[(v - Rl.K.vs)^T(v - Rl.K.vs)]
* the decomposition can be achieved via svd where R = lambdas*(eigen_vectors)^(-1)
* The style augmented process is done on each layer of convolutional auto-encoder with information preservation pooling.
